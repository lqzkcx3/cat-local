package com.fulizhe.catlocal.integration.springboot.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fulizhe.catlocal.integration.springboot.config.CatServiceAnnotation;
import com.fulizhe.catlocal.integration.springboot.mapper.UserMapper;
import com.fulizhe.catlocal.integration.springboot.service.IHelloService;

import cn.hutool.core.util.RandomUtil;

@Service
public class HelloServiceImpl implements IHelloService {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private UserMapper userMapper;

	@CatServiceAnnotation
	@Override
	public List<Map<String, Object>> hello(final String randomString) {
		LoggerFactory.getLogger(this.getClass()).info("开始查询数据库, 参数为 {}", "name = " + randomString);
		userMapper.query(randomString);

		return jdbcTemplate
				.queryForList("SELECT * FROM user WHERE use_name = '" + RandomUtil.randomString(5) + "'" + "");
	}

}
