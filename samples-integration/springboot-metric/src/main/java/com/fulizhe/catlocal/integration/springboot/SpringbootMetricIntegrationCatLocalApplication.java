package com.fulizhe.catlocal.integration.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMetricIntegrationCatLocalApplication {

	public static void main(String[] args) { 
		SpringApplication.run(SpringbootMetricIntegrationCatLocalApplication.class, args);
	}

}
