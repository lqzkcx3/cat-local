package com.fulizhe.catlocal.integration.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dianping.cat.local.integration.mybatis.CatMybatisPlugin;
import com.dianping.cat.local.persistence.CatLogService;
import com.dianping.cat.message.spi.MessageTree;
import com.fulizhe.catlocal.integration.springboot.entity.CatLogInfoEntity;
import com.fulizhe.catlocal.integration.springboot.mapper.CatLogInfoMapper;

@Configuration
public class CatConfig {
	// @Bean
	// public FilterRegistrationBean catFilter() {
	// FilterRegistrationBean registration = new FilterRegistrationBean();
	// final CatFilterDecorator filter = new CatFilterDecorator(true) {
	//
	// };
	// registration.setFilter(filter);
	// registration.addUrlPatterns("/*");
	// registration.setName("cat-filter");
	// registration.setOrder(1);
	// return registration;
	// }

	@Bean
	public CatMybatisPlugin catMybatisPlugin() {
		return new CatMybatisPluginEx();
	}

	@Bean
	public CatLogService catLogService(CatLogInfoMapper catLogInfoMapper) {
		return new CatLogService(null) {
			// ============= 保存的操作交给了 DefaultCatLocalMessageTreeDealer3.java 
//			@Override
//			public void saveLog(MessageTree tree) {
//				super.saveLog(tree);
//			}

			@Override
			public MessageTree find(String traceId) {
				CatLogInfoEntity queryById = catLogInfoMapper.queryById(traceId);
				MessageTree asTo = CatSupport.asTo(queryById.getLoginfo());
				return asTo;
			}
		};
	}
}
