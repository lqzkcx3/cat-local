package com.fulizhe.catlocal.integration.springboot.metric;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.dianping.cat.Cat;
import com.dianping.cat.CatConstants;
import com.dianping.cat.message.Message;

/**
 * 记录入参，出参
 * 
 * @author LQ
 *
 */
//@Configuration
public class ParamAndRvRecordByCatHandlerInterceptor implements HandlerInterceptor, WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		WebMvcConfigurer.super.addInterceptors(registry);

		registry.addInterceptor(this);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		final CacheHttpServletRequestWrapper bladeRequest = new CacheHttpServletRequestWrapper(request);

		final Object requestParams = getRequestParams(request);
		Cat.logEvent(CatConstants.TYPE_URL, CatConstants.TYPE_URL + ".param", Message.SUCCESS,
				requestParams.toString());

		return HandlerInterceptor.super.preHandle(bladeRequest, response, handler);
	}

	private Object getRequestParams(final HttpServletRequest request) {
		return WebUtil.getRequestContent(request);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		if (ex == null) {

		} else {

		}
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}

}
