package com.fulizhe.catlocal.integration.springboot.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@CatAnnotation(layer="Service", description="服务层")
public @interface CatServiceAnnotation {	
}