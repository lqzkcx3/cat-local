package com.fulizhe.catlocal.integration.springboot.config;

import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

public class Point {

	/**
	 * 直连的点
	 */
	private final List<Line> lines = new ArrayList<>();

	private String label;
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private boolean success;

	public void addLine(Line point) {
		lines.add(point);
	}

	public List<Line> getLines() {
		return lines;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return ObjectUtil.toString(BeanUtil.beanToMap(this));
	}

}
