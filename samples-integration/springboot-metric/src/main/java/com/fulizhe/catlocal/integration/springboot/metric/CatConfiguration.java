package com.fulizhe.catlocal.integration.springboot.metric;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CatConfiguration {

	@Bean
	public FilterRegistrationBean<CatMetricFilter> catFilter2() {
		FilterRegistrationBean<CatMetricFilter> registration = new FilterRegistrationBean<CatMetricFilter>();
		final CatMetricFilter filter = new CatMetricFilter();
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		registration.setName("cat-metric-filter");
		registration.setOrder(2);
		return registration;
	}

}