package com.fulizhe.catlocal.integration.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import com.fulizhe.catlocal.integration.springboot.mapper.CatLogInfoMapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;

@Configuration
public class CronConfig {

	@EventListener
	public void refreshEvent(ContextRefreshedEvent event) {
		   CatLogInfoMapper catLogInfoMapper = event.getApplicationContext().getBean(CatLogInfoMapper.class);

		// https://www.bookstack.cn/read/hutool/0f082d6e35363da6.md
		// 【全局定时任务-CronUtil】
		// https://www.bejson.com/othertools/cron/ 【Cron表达式生成器】
		// 每周一早八点 , 删除超过7天的日志记录
		CronUtil.schedule("0 0 8 ? * 2", new Task() {

			@Override
			public void execute() {
				catLogInfoMapper.deleteBeforeDate(DateUtil.offsetDay(DateUtil.date().toJdkDate(), -7));
			}

		});

		CronUtil.start();
	}

}
