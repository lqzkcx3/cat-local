package com.fulizhe.catlocal.integration.springboot.entity;

import java.util.Date;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

public class CatLogInfoEntity {
	private String traceid;
	private String typee;
	private int elapse;
	private String loginfo;
	private String param;
	private String result;
	private String url;
	private Date datee;

	public String getTraceid() {
		return traceid;
	}

	public void setTraceid(String traceid) {
		this.traceid = traceid;
	}

	public String getTypee() {
		return typee;
	}

	public void setTypee(String typee) {
		this.typee = typee;
	}

	public int getElapse() {
		return elapse;
	}

	public void setElapse(int elapse) {
		this.elapse = elapse;
	}

	public String getLoginfo() {
		return loginfo;
	}

	public void setLoginfo(String loginfo) {
		this.loginfo = loginfo;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Date getDatee() {
		return datee;
	}

	public void setDatee(Date datee) {
		this.datee = datee;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return ObjectUtil.toString(BeanUtil.beanToMap(this));
	}

}
