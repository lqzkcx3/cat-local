package com.fulizhe.catlocal.integration.springboot.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dianping.cat.Cat;
import com.fulizhe.catlocal.integration.springboot.config.CatControllerAnnotation;
import com.fulizhe.catlocal.integration.springboot.service.IHelloService;

import cn.hutool.core.util.RandomUtil;

@RestController
public class HelloController {

	@Autowired
	private IHelloService helloSerivce;

	@CatControllerAnnotation
	@GetMapping("/hello")
	public String index(HttpServletRequest request) {
		String randomString = RandomUtil.randomString(5);
		LoggerFactory.getLogger(this.getClass()).info("接受到参数 {}", "name = " + randomString);

		helloSerivce.hello(randomString);

		return String.format(
				"<br><br> 当前请求的TraceId为 [ <strong> <a target=\"_blank\" href=\"./cat/cat.html?traceId=%s\">%s</a> </strong> ] <--- 点击左侧链接跳转到'CAT-LOCAL链路查询页面'查看链路日志 <br><br><br> %s", //
				Cat.getCurrentMessageId(), Cat.getCurrentMessageId(),
				"查看traceId来源, 请打开F12, 然后刷新页面, 在返回HTTP Header中有一个名为X-ROOT-TRACE-ID的key");
	}

	@CatControllerAnnotation
	@GetMapping("/hello2")
	public String helloFail(HttpServletRequest request) {
		String randomString = RandomUtil.randomString(5);
		LoggerFactory.getLogger(this.getClass()).info("接受到参数 {}", "name = " + randomString);

		helloSerivce.hello(randomString);

		throw new RuntimeException("抛出一异常， 验证flow chart");
	}

}