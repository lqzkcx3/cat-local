package com.fulizhe.catlocal.integration.springboot.config;

public class Line {
	private Point leftPoint;
	private Point rightPoint;

//	public Line(Point leftPoint, Point rightPoint) {
//		super();
//		this.leftPoint = leftPoint;
//		this.rightPoint = rightPoint;
//	}
	
	public Line( Point rightPoint) {
		super();
		//this.leftPoint = leftPoint;
		this.rightPoint = rightPoint;
	}

	public Point getLeftPoint() {
		return leftPoint;
	}

	public void setLeftPoint(Point leftPoint) {
		this.leftPoint = leftPoint;
	}

	public Point getRightPoint() {
		return rightPoint;
	}

	public void setRightPoint(Point rightPoint) {
		this.rightPoint = rightPoint;
	}
}
