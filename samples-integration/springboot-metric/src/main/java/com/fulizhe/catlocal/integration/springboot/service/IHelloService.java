package com.fulizhe.catlocal.integration.springboot.service;

import java.util.List;
import java.util.Map;

public interface IHelloService {
	List<Map<String, Object>> hello(final String randomString);
}
