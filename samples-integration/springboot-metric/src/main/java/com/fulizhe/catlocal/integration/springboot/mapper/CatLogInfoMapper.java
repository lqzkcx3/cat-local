package com.fulizhe.catlocal.integration.springboot.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.fulizhe.catlocal.integration.springboot.entity.CatLogInfoEntity;

@Mapper
public interface CatLogInfoMapper {
	@Select("SELECT * FROM cat_log_info WHERE traceid = #{traceId}")
	public CatLogInfoEntity queryById(@Param("traceId") String traceId);			
	
	@Insert("INSERT INTO cat_log_info (traceid, typee, elapse, datee, url, param, result, loginfo) VALUES (#{traceid},#{typee},#{elapse},#{datee},#{url},#{param},#{result},#{loginfo})")
	public int insert(CatLogInfoEntity entity);
	
	@Select("SELECT COUNT(*) FROM cat_log_info")
	public int count();

	/**
	 * @param startIndex 分页查询的起始索引, 0开始
	 * @param rows 本次意图获取到的个数, 默认为10
	 * @return
	 */
	@Select("SELECT * FROM cat_log_info WHERE 1 = 1 ORDER BY datee DESC LIMIT #{startIndex} , #{rows}")	
	public List<CatLogInfoEntity> listByPage(@Param("startIndex")int startIndex, @Param("rows")int rows);
	
	
	@Delete("DELETE FROM cat_log_info WHERE datee < #{date}")
	public int deleteBeforeDate(@Param("date") Date date);	
}
