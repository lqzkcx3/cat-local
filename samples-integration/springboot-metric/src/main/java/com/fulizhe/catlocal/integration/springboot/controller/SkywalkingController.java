package com.fulizhe.catlocal.integration.springboot.controller;

import java.io.File;
import java.util.List;
import java.util.Objects;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.io.FileUtil;

@RestController
public class SkywalkingController {

	@PostMapping("/skywalking_alert")
	public void skywalkingAlert(@RequestBody List<Object> params) {
		LoggerFactory.getLogger(this.getClass()).info("接受到参数 {}", params);

		File file = FileUtil.file("/root/zzzzLQ.txt");		
		FileUtil.touch(file);		
		FileUtil.appendUtf8String(Objects.toString(params), file);
		FileUtil.appendUtf8String(System.lineSeparator(), file);

	}
}