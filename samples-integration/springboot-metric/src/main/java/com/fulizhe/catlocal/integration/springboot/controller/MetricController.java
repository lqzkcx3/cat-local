package com.fulizhe.catlocal.integration.springboot.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.WebUtils;

import com.dianping.cat.message.Message;
import com.dianping.cat.message.Transaction;
import com.dianping.cat.message.spi.MessageTree;
import com.fulizhe.catlocal.integration.springboot.config.CatControllerAnnotation;
import com.fulizhe.catlocal.integration.springboot.config.CatSupport;
import com.fulizhe.catlocal.integration.springboot.config.Line;
import com.fulizhe.catlocal.integration.springboot.config.Point;
import com.fulizhe.catlocal.integration.springboot.mapper.CatLogInfoMapper;

import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONUtil;

@RestController
public class MetricController {

	@Autowired
	private CatLogInfoMapper mapper;

	@GetMapping("/find")
	public String find(HttpServletRequest request) {
		final String traceid = WebUtils.findParameterValue(request, "traceId");
		return mapper.queryById(traceid).toString();
	}

	@GetMapping("/find2")
	public String find2(HttpServletRequest request) {
		final String traceid = WebUtils.findParameterValue(request, "traceId");
		String loginfo = mapper.queryById(traceid).getLoginfo();
		MessageTree asTo = CatSupport.asTo(loginfo);
		Point point = new Point();

		loopMessage(asTo.getMessage(), point);

		Map<Object, Object> rv = MapUtil.builder()//
				.put("url", asTo.getMessage().getName())//
				.put("flowdata", point).build();
		return JSONUtil.toJsonStr(rv);

	}

	void loopMessage(Message message, Point point) {
		if (message instanceof Transaction) {
			Transaction transaction = (Transaction) message;
			List<Message> children = transaction.getChildren();
			// 第一个Event 记住Layer名称
			Message layer = children.get(0);
			String layerName = layer.getName();
			point.setLabel(layerName.split(";")[0]);
			point.setSuccess(transaction.isSuccess());
			if (layerName.contains(";")) {
				point.setDescription(layerName.split(";")[1]);
			}

			if (children.isEmpty()) {
			} else {
				int len = children.size();
				for (int i = 0; i < len; i++) {
					Message child = children.get(i);
					final Point rightPoint = new Point();
					if (child != null) {
						if (child instanceof Transaction) {
							point.addLine(new Line(rightPoint));
						}
						loopMessage(child, rightPoint);
					}
				}

			}
		}
	}

	enum Policy {
		DEFAULT,

		WITHOUT_STATUS,

		WITH_DURATION;

		public static Policy getByMessageIdentifier(byte identifier) {
			switch (identifier) {
			case 't':
				return WITHOUT_STATUS;
			case 'T':
			case 'A':
				return WITH_DURATION;
			case 'E':
			case 'H':
				return DEFAULT;
			default:
				return DEFAULT;
			}
		}
	}

	@CatControllerAnnotation
	@GetMapping("/list")
	public Object list(int rows, int page, String sortOrder, HttpServletRequest request) {

		final String rnd = WebUtils.findParameterValue(request, "rnd");
		final String _ = WebUtils.findParameterValue(request, "_");

		final int startIndex = (page - 1) * rows;
		Map<Object, Object> result = MapUtil.builder().put("rows", mapper.listByPage(startIndex, rows))
				.put("total", mapper.count()).build();
		return result;
	}
}