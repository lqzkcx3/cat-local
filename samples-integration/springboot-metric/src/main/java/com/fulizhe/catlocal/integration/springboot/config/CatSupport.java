package com.fulizhe.catlocal.integration.springboot.config;

import java.io.UnsupportedEncodingException;

import com.dianping.cat.internal.shaded.io.netty.buffer.ByteBuf;
import com.dianping.cat.internal.shaded.io.netty.buffer.Unpooled;
import com.dianping.cat.message.spi.MessageTree;
import com.dianping.cat.message.spi.codec.PlainTextMessageCodec;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.CharsetUtil;

public class CatSupport {

	public static MessageTree asTo(final String loginfo) {
		byte[] bytes;
		try {
			bytes = ("PT1" + '\t' + loginfo).getBytes(CharsetUtil.UTF_8);
		} catch (UnsupportedEncodingException e) {
			throw ExceptionUtil.wrapRuntime(e);
		}
		ByteBuf buf = null;
		try {
			buf = Unpooled.wrappedBuffer(bytes);
			final PlainTextMessageCodec ptmc = new PlainTextMessageCodec();
			return ptmc.decode(buf);
		} catch (Exception e) {
			throw ExceptionUtil.wrapRuntime(e);
		} finally {
			if (null != buf) {
				buf.release();
			}
		}
	}
}
