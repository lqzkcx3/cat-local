package com.fulizhe.catlocal.integration.springboot;

import java.util.List;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fulizhe.catlocal.integration.springboot.entity.CatLogInfoEntity;
import com.fulizhe.catlocal.integration.springboot.mapper.CatLogInfoMapper;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.util.IdUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatLogInfoMapperTests {

	@Autowired
	private CatLogInfoMapper mapper;
	
	@org.junit.Test
	@Ignore
	public void list() {
		Console.log(mapper.listByPage(1,2));
	}
	
	@org.junit.Test
	@Ignore
	public void insert() {
		final CatLogInfoEntity entity = new CatLogInfoEntity();
		entity.setTraceid(IdUtil.fastSimpleUUID());		
		entity.setTypee("TYPEEE");
		entity.setUrl("URL");
		entity.setLoginfo("LOG_INFO");
		entity.setDatee(DateUtil.date().toJdkDate());
		Console.log(mapper.insert(entity));
		List<CatLogInfoEntity> listByPage = mapper.listByPage(1,3);
		Console.log(listByPage);		
	}
	
	@org.junit.Test
	public void delete() {
		insert();
		mapper.deleteBeforeDate(DateUtil.offsetDay(DateUtil.date().toJdkDate(), -1));
		Console.log(mapper.listByPage(1,1));
	}		

}
