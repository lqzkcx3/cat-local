package com.fulizhe.catlocal.integration.struts2;

import com.jfinal.core.JFinal;

/**
 * 应用启动入口; 推荐放在 src/test/java目录下
 * 
 * @author LQ
 *
 */
public class JFainlAppRun {

	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目 运行此 main
	 * 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {

		// 第一个参数可以是 绝对路径
		JFinal.start("src/main/webapp", 80, "/", 5);
		
		browse("http://localhost");
	}

	private static void browse(final String url) {
		// 判断当前系统是否支持Java AWT Desktop扩展
		if (java.awt.Desktop.isDesktopSupported()) {
			try {
				// 创建一个URI实例,注意不是URL
				java.net.URI uri = java.net.URI.create(url);
				// 获取当前系统桌面扩展
				java.awt.Desktop dp = java.awt.Desktop.getDesktop();
				// 判断系统桌面是否支持要执行的功能
				if (dp.isSupported(java.awt.Desktop.Action.BROWSE)) {
					// 获取系统默认浏览器打开链接
					dp.browse(uri);
				}
			} catch (java.lang.NullPointerException e) {
				// 此为uri为空时抛出异常
			} catch (java.io.IOException e) {
				// 此为无法获取系统默认浏览器
			}
		}
	}
}