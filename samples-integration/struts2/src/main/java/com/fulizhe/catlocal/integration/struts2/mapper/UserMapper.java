package com.fulizhe.catlocal.integration.struts2.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
	@Select("SELECT * FROM user WHERE use_name = #{userName}")
	public Map<String, Object> query(@Param("userName") String userName);
	
/*
 * struts2

	http://struts.apache.org/getting-started/how-to-create-a-struts2-web-application.html#to-run-the-application-using-maven-add-the-jetty-maven-plugin-to-your-pomxml

 * */	
}
