package com.fulizhe.catlocal.integration.struts2.action;

import java.io.IOException;
import java.io.StringBufferInputStream;

import org.apache.struts2.ServletActionContext;
import org.slf4j.LoggerFactory;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.mybatis.CatMybatisPlugin;
import com.dianping.cat.util.Files;
import com.fulizhe.catlocal.integration.struts2.MyBatisUtil;
import com.fulizhe.catlocal.integration.struts2.mapper.UserMapper;

public class HelloAction {

	public void hello() {
		LoggerFactory.getLogger(this.getClass()).info("开始查询数据库, 参数为 {}", "name = 钱二");

		MyBatisUtil.getMapper(UserMapper.class).query("钱二");

		final String ri = String.format(
				"<br><br> 当前请求的TraceId为 [ <strong> <a target=\"_blank\" href=\"./cat/cat.html?traceId=%s\">%s</a> </strong> ] <--- 点击左侧链接跳转到'CAT-LOCAL链路查询页面'查看链路日志", //
				Cat.getCurrentMessageId(), Cat.getCurrentMessageId());

		try {
			ServletActionContext.getResponse().setCharacterEncoding("GBK");
			ServletActionContext.getResponse().setContentType("text/html;charset=GBK");
			Files.forIO().copy(new StringBufferInputStream(ri), ServletActionContext.getResponse().getOutputStream());
		} catch (IOException e) { 
			throw new RuntimeException(e);
		}
	}

}