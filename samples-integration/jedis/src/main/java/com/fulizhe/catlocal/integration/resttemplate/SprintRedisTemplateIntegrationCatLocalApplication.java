package com.fulizhe.catlocal.integration.resttemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintRedisTemplateIntegrationCatLocalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintRedisTemplateIntegrationCatLocalApplication.class, args);
	}

}
