package com.fulizhe.catlocal.integration.springboot.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis配置类, 本配置与 CAT-LOCAL 无关 
 *
 * @author LQ
 */
@Configuration
@MapperScan(basePackages = { "com.fulizhe.catlocal.integration.springboot.mapper" })
public class MybatisPlusConfiguration {
	
}
