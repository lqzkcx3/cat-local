
## 1. 概述
本文档阐述如何在Apache HttpClient組件中集成 CAT-LOCAL.

用到的技术:
1. Apache HttpClient

## 2. 步骤
1. `ApacheHttpClientIntegrationCatLocalApplication`中注冊`HttpCatCrossFliter`.
2. Apache HttpClient組件集成参见`HelloController`

### 2.1 引入依赖
[引入本CAT-LOCAL依赖](./../../doc/import-dependency.md)

### 2.2 配置工作(CAT官方要求)
在你的项目中创建 `src/main/resources/META-INF/app.properties` 文件, 并添加如下内容(这一步主要是为了唯一性标识当前系统):

```
app.name={appkey}
```

> appkey 只能包含英文字母 (a-z, A-Z)、数字 (0-9)、下划线 (\_) 和中划线 (-)


### 2.3 配置工作(CAT-CLIENT)
1. 本示例项目下的 `CatStaticResourceConfig` 类. 本配置主要是为了给CAT-LOCAL的内置页面放行.
2. 本示例项目下的配置文件`application.yaml`中启用 `cat.local.enabled=true` . 目前我们将CAT-LOCAL的启用默认关闭, 所以这里还需要将其手工打开.
3. 启用应用, 访问 http://localhost.

## 3. CAT API操作文档
[准备工作](./../../doc/quickstart.md) 中的 API List 部分