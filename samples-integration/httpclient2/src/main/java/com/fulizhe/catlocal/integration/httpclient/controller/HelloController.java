package com.fulizhe.catlocal.integration.httpclient.controller;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dianping.cat.Cat;
import com.fulizhe.catlocal.integration.httpclient.HttpUtil;

@RestController
public class HelloController {

	@Autowired
	private ServerProperties sp;

	@GetMapping("/hello")
	public String index(HttpServletRequest request) {
		LoggerFactory.getLogger(this.getClass()).info("开始Apache HttpClient调用....");

		httpTest();

		return String.format(
				"<br><br> 当前请求的TraceId为 [ <strong> <a target=\"_blank\" href=\"./cat/cat.html?traceId=%s\">%s</a> </strong> ] <--- 点击左侧链接跳转到'CAT-LOCAL链路查询页面'查看链路日志 <br><br><br> %s", //
				Cat.getCurrentMessageId(), Cat.getCurrentMessageId(),
				"查看traceId来源, 请打开F12, 然后刷新页面, 在返回HTTP Header中有一个名为X-ROOT-TRACE-ID的key");
	}

	@GetMapping("/calledRemoteService")
	public String index2(HttpServletRequest request) {
		Cat.getManager().setTraceMode(true);
		LoggerFactory.getLogger(this.getClass()).info("被Apache HttpClient Invoke....");

		return "Invoked By Apache HttpClient";
	}

	String httpTest() {
		final String url = String.format("http://127.0.0.1:%s/calledRemoteService", sp.getPort());
		return HttpUtil.get(url, Collections.emptyMap(), Collections.emptyMap());
	}
}