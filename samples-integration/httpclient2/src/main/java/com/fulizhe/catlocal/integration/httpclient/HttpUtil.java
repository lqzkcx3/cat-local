package com.fulizhe.catlocal.integration.httpclient;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.web.multipart.MultipartFile;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.servlet.CatMsgConstants;
import com.dianping.cat.local.integration.servlet.CatMsgContext;

public abstract class HttpUtil {
	public static String get(String baseUrl, Map<String, Object> hearder, Map<String, Object> getBody) {
		HttpClient client = new HttpClient();
		GetMethod myGet = null;
		String result = "";
		String getString = "";
		// 设置请求超时时间
		try {
			// 设置请求体
			if (getBody != null) {
				for (String key : getBody.keySet()) {
					String value = getBody.get(key).toString();
					getString += key + "=" + URLEncoder.encode(value) + "&";
				}
			}

			if (!getString.equals("")) {
				getString = getString.substring(0, getString.length() - 1);
			}
			if (!getString.equals("")) {
				baseUrl = baseUrl + "?" + getString;
			}
			myGet = new GetMethod(baseUrl);
			appendCatRemoteCallHeaders(myGet);
			client.getHttpConnectionManager().getParams().setConnectionTimeout(300000);

			// 设置请求头部类型
			for (String key : hearder.keySet()) {
				String value = hearder.get(key).toString();
				myGet.setRequestHeader(key, value);
			}

			int statusCode = client.executeMethod(myGet);
			// if (statusCode == HttpStatus.SC_OK) {
			BufferedInputStream bis = new BufferedInputStream(myGet.getResponseBodyAsStream());
			byte[] bytes = new byte[1024];
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int count = 0;
			while ((count = bis.read(bytes)) != -1) {
				if (count == 0) {
					break;
				}
				bos.write(bytes, 0, count);
			}
			byte[] strByte = bos.toByteArray();
			result = new String(strByte, 0, strByte.length, "utf-8");
			bos.close();
			bis.close();
			// }
		} catch (Exception e) {
			result = "发送失败！" + e.getMessage();
		} finally {
			// httpClient.getConnectionManager().shutdown(); //关闭这里的实例
			if (myGet != null) {
				myGet.releaseConnection();
			}
		}
		return result;
	}

	private static void appendCatRemoteCallHeaders(HttpMethodBase httpMethodBase) {
		final CatMsgContext context = new CatMsgContext();
		Cat.logRemoteCallClient(context);

		httpMethodBase.setRequestHeader(Cat.Context.ROOT, context.getProperty(Cat.Context.ROOT));
		httpMethodBase.setRequestHeader(Cat.Context.CHILD, context.getProperty(Cat.Context.CHILD));
		httpMethodBase.setRequestHeader(Cat.Context.PARENT, context.getProperty(Cat.Context.PARENT));
		httpMethodBase.setRequestHeader(CatMsgConstants.APPLICATION_KEY, Cat.getManager().getDomain());

	}
}
