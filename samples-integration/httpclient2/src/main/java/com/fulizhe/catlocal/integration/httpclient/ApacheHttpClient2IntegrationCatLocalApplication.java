package com.fulizhe.catlocal.integration.httpclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.dianping.cat.local.integration.servlet.HttpCatCrossFliter;

@SpringBootApplication
public class ApacheHttpClient2IntegrationCatLocalApplication {

	@Bean
	public FilterRegistrationBean<HttpCatCrossFliter> catFilterRemote() {
		final FilterRegistrationBean<HttpCatCrossFliter> registration = new FilterRegistrationBean<>();
		final HttpCatCrossFliter filter = new HttpCatCrossFliter();
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		registration.setName("cat-filter-remote-call");
		registration.setOrder(2);
		return registration;
	}

	public static void main(String[] args) {
		SpringApplication.run(ApacheHttpClient2IntegrationCatLocalApplication.class, args);
	}

}
