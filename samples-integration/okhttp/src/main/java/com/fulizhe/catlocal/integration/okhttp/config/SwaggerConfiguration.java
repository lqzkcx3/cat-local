package com.fulizhe.catlocal.integration.okhttp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dianping.cat.local.integration.springmvc.CatController;
import com.fulizhe.catlocal.integration.okhttp.controller.HelloController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * Swagger配置类, 和CAT-LOCAL无密切关系, 只是为了更好地展示效果
 *
 * @author Chill
 */
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfiguration {

	/**
	 * 引入Knife4j扩展类
	 */
	//@Autowired
	//private OpenApiExtensionResolver openApiExtensionResolver;

	@Bean
	public Docket catDocket() {
		return new Docket(DocumentationType.SWAGGER_2)//
				.groupName("CAT模块")//
				.select().apis(s -> s.declaringClass().equals(CatController.class))//
				.paths(PathSelectors.any()).build();
	}

	@Bean
	public Docket helloDocket() {
		return new Docket(DocumentationType.SWAGGER_2)//
				.groupName("HelloWorld模块")//
				.select().apis(s -> s.declaringClass().equals(HelloController.class))//
				.paths(PathSelectors.any()).build();
	}

}
