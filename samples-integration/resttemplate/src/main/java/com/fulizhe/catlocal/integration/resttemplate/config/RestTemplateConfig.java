package com.fulizhe.catlocal.integration.resttemplate.config;

import java.io.IOException;
import java.nio.charset.Charset;

import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.servlet.CatMsgConstants;
import com.dianping.cat.local.integration.servlet.CatMsgContext;

@Configuration
public class RestTemplateConfig {
	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
		RestTemplate restTemplate = new RestTemplate(factory);

		// restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
		//
		// @Override
		// public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
		// throws IOException {
		//
		// createMessageTree();
		//
		// final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		// final String rootId = requestAttributes.getAttribute(Cat.Context.ROOT, 0).toString();
		// final String childId = requestAttributes.getAttribute(Cat.Context.CHILD, 0).toString();
		// final String parentId = requestAttributes.getAttribute(Cat.Context.PARENT, 0).toString();
		//
		//
		// request.getHeaders().add(Cat.Context.ROOT, rootId);
		// request.getHeaders().add(Cat.Context.CHILD, childId);
		// request.getHeaders().add(Cat.Context.PARENT, parentId);
		// request.getHeaders().add(CatMsgConstants.APPLICATION_KEY, Cat.getManager().getDomain());
		// return execution.execute(request, body);
		// }
		// });

		restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {

			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			      throws IOException {
				final CatMsgContext context = new CatMsgContext();
				Cat.logRemoteCallClient(context);

				request.getHeaders().add(Cat.Context.ROOT, context.getProperty(Cat.Context.ROOT));
				request.getHeaders().add(Cat.Context.CHILD, context.getProperty(Cat.Context.CHILD));
				request.getHeaders().add(Cat.Context.PARENT, context.getProperty(Cat.Context.PARENT));
				request.getHeaders().add(CatMsgConstants.APPLICATION_KEY, Cat.getManager().getDomain());
				return execution.execute(request, body);
			}
		});

		// 支持中文编码
		restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		return restTemplate;
	}
//
//	/**
//	 * 统一设置消息编号的messageId
//	 */
//	private void createMessageTree() {
//		LoggerFactory.getLogger(this.getClass()).debug("统一设置消息编号的messageId");
//		final CatMsgContext context = new CatMsgContext();
//		Cat.logRemoteCallClient(context);
//		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//		requestAttributes.setAttribute(Cat.Context.PARENT, context.getProperty(Cat.Context.PARENT), 0);
//		requestAttributes.setAttribute(Cat.Context.ROOT, context.getProperty(Cat.Context.ROOT), 0);
//		requestAttributes.setAttribute(Cat.Context.CHILD, context.getProperty(Cat.Context.CHILD), 0);
//		requestAttributes.setAttribute(CatMsgConstants.APPLICATION_KEY, Cat.getManager().getDomain(), 0);
//	}

	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setReadTimeout(5000);// 单位为ms
		factory.setConnectTimeout(5000);// 单位为ms
		return factory;
	}
}