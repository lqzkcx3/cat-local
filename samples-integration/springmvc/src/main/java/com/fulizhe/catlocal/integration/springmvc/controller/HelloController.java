package com.fulizhe.catlocal.integration.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dianping.cat.Cat;
import com.fulizhe.catlocal.integration.springmvc.mapper.UserMapper;

@RestController
public class HelloController {

	@Autowired
	private UserMapper userMapper;

	@GetMapping("/hello")
	public String index(HttpServletRequest request) {
		LoggerFactory.getLogger(this.getClass()).info("开始查询数据库, 参数为 {}", "name = 钱二");
		userMapper.query("钱二");

		return String.format(
				"<br><br> 当前请求的TraceId为 [ <strong> <a target=\"_blank\" href=\"./cat/cat.html?traceId=%s\">%s</a> </strong> ] <--- 点击左侧链接跳转到'CAT-LOCAL链路查询页面'查看链路日志 <br><br><br> %s", //
				Cat.getCurrentMessageId(), Cat.getCurrentMessageId(),
				"查看traceId来源, 请打开F12, 然后刷新页面, 在返回HTTP Header中有一个名为X-ROOT-TRACE-ID的key");
	}

	// 默认首页
	@GetMapping(value = "/")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("1.html");
	}

}