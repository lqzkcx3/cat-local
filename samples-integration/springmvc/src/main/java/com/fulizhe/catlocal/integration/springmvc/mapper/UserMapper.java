package com.fulizhe.catlocal.integration.springmvc.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
	@Select("SELECT * FROM user WHERE use_name = #{userName}")
	public Map<String, Object> query(@Param("userName") String userName);
}
