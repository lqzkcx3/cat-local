package com.fulizhe.catlocal.integration.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootIntegrationCatLocalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootIntegrationCatLocalApplication.class, args);
	}

}
