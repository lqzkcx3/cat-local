
## 1. 概述
本文档阐述如何在SpringBoot项目中集成 CAT-LOCAL.

用到的技术:
1. Swagger 主要是为了读者更方便的操作项目提供的演示接口.
2. Mybatis 演示CAT如何监控数据库操作
3. logback 演示CAT如何收集logback日志

## 2. 步骤
简单的三步, 你的系统将收获:
1. URL请求监控(自动)
2. Mybatis数据库操作监控(自动)
3. JdbcTemplate埋点监控(手动, 相关配置类: [JdbcTemplateAspect.java](src/main/java/com/fulizhe/catlocal/integration/springboot/config/JdbcTemplateAspect.java))
3. logback日志查询监控(需要手动配置下logback.xml文件, 本项目已给出样例)
4. 更多其它组件的集成参见[官方示例](https://gitee.com/mirrors/CAT/tree/master/integration). -- 再次强调: 本组件与CAT官方无缝兼容, 所以这里请放心集成, 后续切换到CAT标准版不需要使用者修改任何代码

### 2.1 引入依赖
[引入本CAT-LOCAL依赖](./../../doc/import-dependency.md)

### 2.2 配置工作(CAT官方要求)
在你的项目中创建 `src/main/resources/META-INF/app.properties` 文件, 并添加如下内容(这一步主要是为了唯一性标识当前系统):

```
app.name={appkey}
```

> appkey 只能包含英文字母 (a-z, A-Z)、数字 (0-9)、下划线 (\_) 和中划线 (-)


### 2.3 配置工作(CAT-CLIENT)
1. 本示例项目下的 `CatStaticResourceConfig` 类. 本配置主要是为了给CAT-LOCAL的内置页面放行.
2. 本示例项目下的配置文件`application.yaml`中启用 `cat.local.enabled=true` . 目前我们将CAT-LOCAL的启用默认关闭, 所以这里还需要将其手工打开.
3. 启用应用, 访问 http://localhost.

## 3. CAT API操作文档
[准备工作](./../../doc/quickstart.md) 中的 API List 部分