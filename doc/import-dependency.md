
## 1. 简介
本文档阐述如何在现有项目中引入本CAT-CLIENT依赖.



### 2 引入依赖(Maven方式)
先[将CAT-LOCAL安装到本地仓库](./../cat-client-java-local/README.md) - 注: 此操作只需要执行一次

```xml
	<dependency>
	    <groupId>com.dianping.cat</groupId>
	    <artifactId>cat-client-local</artifactId>
	    <version>3.0.0</version>
	    <!-- cat~client官方版本会在打包时候将依赖项netty打包到自身的jar中, 秉承尽量少动源代码的思想, 我们不对齐打包逻辑进行修改, 所以这里需要显式排除对于netty的依赖 -->
	    <exclusions>
	    	<exclusion>
	    		<groupId>io.netty</groupId>
	    		<artifactId>netty-all</artifactId>
	    	</exclusion>
	    </exclusions>
	</dependency>
```

### 3. 直接引入依赖JAR
如果没有使用maven管理依赖，拉取本项目后, 在根目录下的 jar/ 文件中存放的正是最新版本的依赖. 直接复制 jar/cat-client-local-x.x.x.jar 到项目 WEB_INF/lib 路径下。(无需担心传递依赖, 正如上面所说的, cat-client将唯一的netty依赖打包到了自身内部).
