# Cat Client Local for Java

## 前言
为现有系统接入监控体系提供最简解决方案。—— 你负责引入JAR依赖, 剩下的工作全部交给我们！

本项目基于大众点评的CAT监控平台开源版的cat-client for java. 意图打造史上最低难度的单体应用监控接入解决方案, 摆脱过往需要登录部署服务器查看日志排错的低效工作方式,  助推监控体系在无专业运维团队的快速落地, 实现系统监控常态化。

[项目产生的背景](https://blog.csdn.net/lqzkcx3/article/details/119839739)

## 使用方法
1. 对于每个前端请求， 返回值的HTTP HEADER中都会存在一个KEY为`X-CAT-ROOT-ID`的键值对, 该键值对的值也就是我们常说的链路追踪体系下的traceId.
2. 拿着上一步获取到的traceId, 即可以在我们提供的默认查询页面下获取所对应的请求全链路调用图(见下方示例GIF).
3. 在自定义查询页面下，我们还提供了下载当前页面，以及将当前页面导出为图片的附加功能，以应对因为某些原因无法在线查询链路日志的问题。

## 效果图
![demo](./doc/demo.gif)

对于以上GIF中链路查询结果的解释:
![demo-explain](./doc/demo-explain.png)


## 演示地址
[演示地址](http://146.56.220.24/)


## 与官方标准版CAT-CLIENT比较
1. 无需要部署服务端，引入本组件依赖就能用。
2. 内置请求链路查询页面。结合本组件自动生成并作为HTTP Header返回前端的traceid，使用者无需任何额外开发即可享用全链路日志追踪的功能。
3. 内置日志文件按小时切割储存，无需额外部署存储介质，同时杜绝了单个日志文件过大的问题。注：这一功能移植自标准版CAT。
4. 内置链路查询结果导出功能，以应对诸如内网隔离等导致的无法在线搜索的问题。
5. 内置链路图形化展示功能，以人类更敏感的图形化方式更加直观地展示链路中各操作的耗时占比。注：这一功能移植自标准版CAT。
6. 提供对外扩展接口。使用者可以籍此实现诸如报表统计、预警通知、流量回放、规范落地审查等等自定义需求（参见下方集成样例）。
7. 提供完善的入门文档和典型示例项目, 极限压低接入曲线。


## 集成样例
1. [springboot项目集成](./samples-integration/springboot/README.md)
2. [springmvc项目集成](./samples-integration/springmvc/README.md)
3. [struts2项目集成](./samples-integration/struts2/README.md)
4. [Spring-JdbcTemplate埋点](./samples-integration/springboot/README.md)
4. [Spring-RestTemplate埋点](https://gitee.com/lqzkcx3/cat-local/issues/I48NES)
4. [Spring-RedisTemplate埋点](https://gitee.com/lqzkcx3/cat-local/issues/I4A90D)
6. [MybatisPlus埋点](https://gitee.com/lqzkcx3/cat-local/issues/I472WH)
7. [OkHttp埋点](https://gitee.com/lqzkcx3/cat-local/issues/I471NN)
8. [Apache HttpClient埋点](https://gitee.com/lqzkcx3/cat-local/issues/I471O1)
9. [commons-httpclient埋点](https://gitee.com/lqzkcx3/cat-local/issues/I4AFM4)
9. [Metric示例](https://gitee.com/lqzkcx3/cat-local/issues/I46VSX)
9. [玩得花之调用链路展示为流程图效果](https://gitee.com/lqzkcx3/cat-local/issues/I4DP3N)
99. [官方集成案例](https://gitee.com/mirrors/CAT/tree/master/integration)


## 原理
1. 直接复用CAT久经考验的日志模型，以及基于ThreadLocal的实现方式, 即不介入cat-client sdk中前期的日志收集和日志结构组装的逻辑，尽可能地站在巨人肩膀上. 
2. 主要的自定义改造点位于cat-client sdk将收集来的日志通过netty发送给服务端的那部分代码实现处。

## FAQ
1. 是否支持跨服务调用?

本组件的最新版是支持跨服务调用的, 但这里并不推荐采用本组件进行跨服务调用的支持。
跨服务推荐使用标准版本，我们这个主打单体应用追踪，将监控集成的成本压到最低，哪怕你是个单人项目也可以从中受益。


## 相关介绍性文档
- [ProcessOn - 与官方开源版本的对比](https://www.processon.com/view/link/6059f3366376897007743b48)
- [【CAT魔改】独立化cat-client](https://blog.csdn.net/lqzkcx3/article/details/115023572)
- [【CAT魔改】独立化cat-client - 扩展功能 - 前后端规范审计](https://blog.csdn.net/lqzkcx3/article/details/115560676)
- [【CAT魔改】扩展cat-consumer实现自定义服务端收集日志](https://blog.csdn.net/lqzkcx3/article/details/117039515)
- [【CAT魔改】为cat-home添加链路追踪查询](https://blog.csdn.net/lqzkcx3/article/details/119778400)

## 快速入门
[快速入门](./doc/quickstart.md)