package com.dianping.cat.local.integration.springboot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.servlet.CatFilterDecorator;
import com.dianping.cat.local.integration.springmvc.CatController;

/**
 * <p>
 * https://github.com/dianping/cat/tree/master/integration/spring-boot
 * <p>
 * 理论: https://github.com/dianping/cat/tree/master/integration/URL
 * 
 * @author LQ
 *
 */
@Configuration
@ConditionalOnClass(value = { Cat.class })
@ConditionalOnProperty(value = "cat.local.enabled", havingValue = "true", matchIfMissing = false)
@Import({ CatLogServiceConfiguration.class, CatController.class })
public class CatAutoConfiguration {

	// 是否启用CAT的日志增强
	@org.springframework.beans.factory.annotation.Value("${cat.local.trace-mode:true}")	
	private boolean enableTraceMode;

	@Bean
	@ConditionalOnMissingBean(name = {"catFilter"})
	public FilterRegistrationBean catFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		final CatFilterDecorator filter = new CatFilterDecorator(enableTraceMode);
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		registration.setName("cat-filter");
		registration.setOrder(1);
		return registration;
	}

}