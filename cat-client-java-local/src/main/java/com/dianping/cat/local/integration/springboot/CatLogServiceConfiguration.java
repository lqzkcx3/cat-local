package com.dianping.cat.local.integration.springboot;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dianping.cat.local.persistence.CatLogService;
import com.dianping.cat.local.persistence.CatLogServiceFactory;

@Configuration
@ConditionalOnProperty(value = "cat.local.enabled", havingValue = "true", matchIfMissing = false)
public class CatLogServiceConfiguration {
	// =========================================================================

	/**
	 * 使用这个实例作为扩展时, 注意搭配 {@code CatLocalMessageTreeDealer};
	 * 
	 * @return
	 */
	@Bean
	@ConditionalOnMissingBean(name = "catLogService")
	public CatLogService catLogService() {
		return CatLogServiceFactory.getSingleton();
	}
}