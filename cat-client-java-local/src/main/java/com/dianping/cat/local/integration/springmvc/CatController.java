package com.dianping.cat.local.integration.springmvc;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dianping.cat.local.messagecodec.HtmlMessageCodec;
import com.dianping.cat.local.messagecodec.WaterfallMessageCodec;
import com.dianping.cat.local.persistence.CatLogService;
import com.dianping.cat.message.Transaction;
import com.dianping.cat.message.spi.MessageTree;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

@RestController
@RequestMapping("/cat/")
public class CatController {

	private final WaterfallMessageCodec m_waterfall = new WaterfallMessageCodec();

	private final HtmlMessageCodec m_html = new HtmlMessageCodec();

	@Autowired
	private CatLogService catLogService;

	@PostConstruct
	public void init() {
		m_html.setLogViewPrefix("/cat/catLog/");
	}

	@GetMapping(value = "catLog")
	public void catLog(@RequestParam("traceId") String traceId, HttpServletResponse response) {
		doDealCatLog(traceId, "true".equalsIgnoreCase(getRequest().getParameter("waterfall")), response);
	}

	@GetMapping(value = "catLog/{traceId}")
	public void catLog2(@PathVariable("traceId") String traceId, HttpServletResponse response) {
		doDealCatLog(traceId, "true".equalsIgnoreCase(getRequest().getParameter("waterfall")), response);
	}

	private void doDealCatLog(String traceId, boolean waterfall, HttpServletResponse response) {

		final MessageTree mt = catLogService.find(traceId);
		writeContentToResponse(encodeTxt(mt, waterfall), response);
	}

	private String encodeTxt(MessageTree tree, boolean waterfall) {
		if (tree != null) {
			ByteBuf buf = ByteBufAllocator.DEFAULT.buffer(8192);

			if (tree.getMessage() instanceof Transaction && waterfall) {
				m_waterfall.encode(tree, buf);
			} else {
				m_html.encode(tree, buf);
			}

			try {
				buf.readInt(); // get rid of length
				return buf.toString(Charset.forName("utf-8"));
			} catch (Exception e) {
				// ignore it
			} finally {
				if (null != buf) {
					buf.release();
				}
			}
		}

		return "";
	}

	private void writeContentToResponse(String content, HttpServletResponse response) {
		// response.setContentType("application/xhtml+xml;charset=utf-8");
		response.setCharacterEncoding("utf-8");
		try (BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream())) {
			byte[] bytes = content.getBytes();
			bos.write(bytes, 0, bytes.length);
			bos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "catTest")
	public String catTest(@RequestParam("isThrow") boolean isThrow) {
		LoggerFactory.getLogger(this.getClass()).info("进行CAT测试, 传入参数为: {} ", getRequest().getParameterMap());
		if (isThrow) {
			throw new RuntimeException("抛出異常, 验证CAT全链路日志");
		} else {
			return "调用成功";
		}

	}

	/**
	 * 获取 HttpServletRequest
	 *
	 * @return {HttpServletRequest}
	 */
	static HttpServletRequest getRequest() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		return (requestAttributes == null) ? null : ((ServletRequestAttributes) requestAttributes).getRequest();
	}
}