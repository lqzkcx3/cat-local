package com.dianping.cat.local.persistence;

class ServerConfigManager {
	public static final String DUMP_DIR = "dump";

	public boolean isUseNewStorage() {
		// return Boolean.parseBoolean(getProperty("use-new-storage", "true"));
		return false;
	}

	public String getHdfsLocalBaseDir(String id) {
		// /data/appdatas/cat/bucket/

		// if (id == null) {
		// return "target/bucket";
		// } else {
		// return "target/bucket/" + id;
		// }
		if (id == null) {
			return "cat-local-bucket";
		} else {
			return "cat-local-bucket/";
		}
	}

	public boolean isLocalMode() {
		// return Boolean.parseBoolean(getProperty(LOCAL_MODE, "false"));
		return true;
	}

	public int getBlockDumpThread() {
		// return Integer.parseInt(getProperty("block-dump-thread", "5"));
		return 5;
	}
}
