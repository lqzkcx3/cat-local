package com.dianping.cat.local.integration.springboot;

import org.apache.ibatis.plugin.Interceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.mybatis.CatMybatisPlugin;

@Configuration
@ConditionalOnClass(value = { Cat.class, Interceptor.class })
@ConditionalOnProperty(value = "cat.local.enabled", havingValue = "true", matchIfMissing = false)
public class CatMybatisPluginAutoConfiguration {
	@Bean
	@ConditionalOnMissingBean(name = {"catMybatisPlugin"})
	public CatMybatisPlugin catMybatisPlugin() {
		return new CatMybatisPlugin();
	}
}
