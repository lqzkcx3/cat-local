package com.dianping.cat.local.integration.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dianping.cat.Cat;
import com.dianping.cat.local.integration.springboot.CatAutoConfiguration;
import com.dianping.cat.message.Event;
import com.dianping.cat.message.Transaction;
import com.dianping.cat.message.internal.AbstractMessage;
import com.dianping.cat.message.internal.NullMessage;
import com.dianping.cat.util.StringUtils;

/**
 * <p>
 * 抓取其它微服务端发来的请求中的 CAT 相关信息, 构建自身的CAT消息树模型
 * <p>
 * 使用: 在会接收到其它微服务调用的微服务组件上安装, 如果该组件不会接收到调用, 可以不装.
 * <p>
 * 
 * @see CatAutoConfiguration 其中注冊的 CatFilter
 * @author LQ
 *
 */
public class HttpCatCrossFliter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(HttpCatCrossFliter.class);

	static final String DEFAULT_APPLICATION_NAME = "default";

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) req;
		final String requestURI = request.getRequestURI();
		if (StringUtils.isEmpty(request.getHeader(Cat.Context.ROOT))) {
			logger.debug("{},为非CAT调用, 直接放行", requestURI);
			filterChain.doFilter(req, resp);
			return;
		}

		// 构建CAT消息树模型
		final Transaction t = Cat.newTransaction(CatMsgConstants.CROSS_SERVER, requestURI);

		try {
			Cat.Context context = new CatMsgContext();
			context.addProperty(Cat.Context.ROOT, request.getHeader(Cat.Context.ROOT));
			context.addProperty(Cat.Context.PARENT, request.getHeader(Cat.Context.PARENT));
			context.addProperty(Cat.Context.CHILD, request.getHeader(Cat.Context.CHILD));
			Cat.logRemoteCallServer(context);

			this.createProviderCross(request, t);

			// 执行链向前推进
			filterChain.doFilter(req, resp);

			t.setStatus(Transaction.SUCCESS);
		} catch (Exception e) {
			logger.error("------ Get cat msgtree error : ", e);

			Event event = Cat.newEvent("HTTP_REST_CAT_ERROR", requestURI);
			event.setStatus(e);
			completeEvent(event);
			t.addChild(event);
			t.setStatus(e.getClass().getSimpleName());
		} finally {
			t.complete();
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

	/**
	 * 串联provider端消息树
	 *
	 * @param request
	 * @param t
	 */
	private void createProviderCross(HttpServletRequest request, Transaction t) {
		Event crossAppEvent = Cat.newEvent(CatMsgConstants.PROVIDER_CALL_APP,
				request.getHeader(CatMsgConstants.APPLICATION_KEY)); // clientName
		Event crossServerEvent = Cat.newEvent(CatMsgConstants.PROVIDER_CALL_SERVER, request.getRemoteAddr()); // clientIp
		crossAppEvent.setStatus(Event.SUCCESS);
		crossServerEvent.setStatus(Event.SUCCESS);
		completeEvent(crossAppEvent);
		completeEvent(crossServerEvent);
		t.addChild(crossAppEvent);
		t.addChild(crossServerEvent);
	}

	private void completeEvent(Event event) {
		if (event != NullMessage.EVENT) {
			AbstractMessage message = (AbstractMessage) event;
			message.setCompleted(true);
		}
	}

}