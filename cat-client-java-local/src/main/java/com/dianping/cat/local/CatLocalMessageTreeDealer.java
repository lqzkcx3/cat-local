package com.dianping.cat.local;

import com.dianping.cat.message.spi.MessageTree;

/**
 * 参考
 * @author LQ
 *
 */
public interface CatLocalMessageTreeDealer {
	void deal(MessageTree tree);
}