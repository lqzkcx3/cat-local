package com.dianping.cat.local.persistence;

public class CatLogServiceFactory {

	private CatLogServiceFactory() {
	}

	private static CatLogService cs;

	public static CatLogService getSingleton() {
		if (null == cs) {
			cs = new CatLogService(LocalMessageBucketManager.getInstance());
		}
		return cs;
	}
}
