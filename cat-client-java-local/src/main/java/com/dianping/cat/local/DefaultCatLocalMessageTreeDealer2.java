package com.dianping.cat.local;

import com.dianping.cat.local.persistence.CatLogServiceFactory;
import com.dianping.cat.message.spi.MessageTree;

import lombok.extern.slf4j.Slf4j;

/**
 * 使用CAT官方的日志存储规则进行日志收集, 进而提供日志的对外查询功能
 * 
 * @author LQ
 *
 */
@Slf4j
public class DefaultCatLocalMessageTreeDealer2 implements CatLocalMessageTreeDealer {

	@Override
	public void deal(MessageTree tree) {
		if (log.isDebugEnabled()) {
			log.debug("处理日志: {}", tree);
		}		
		CatLogServiceFactory.getSingleton().saveLog(tree);
	}

}